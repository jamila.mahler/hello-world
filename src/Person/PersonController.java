package Person;
import java.text.NumberFormat;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class PersonController {
	
	public PersonView view;
	public PersonModel model;
	
	public PersonController(PersonView view, PersonModel model) {
		this.view = view;
		this.model = model;
		
		view.nameText.textProperty().addListener((observable, oldValue, newValue) -> changeName(newValue));
		view.ageText.textProperty().addListener((obseravble, oldValue, newValue) -> changeAge(newValue));
		view.marriedText.textProperty().addListener((obseravble, oldValue, newValue) -> divorce(newValue));
		view.button.setOnAction(this::displayName);
	}

	private void divorce(String newValue) {
		model.setMarried(Boolean.parseBoolean(newValue));
	}

	private void changeAge(String newValue) {
		model.setAge(Integer.parseInt(newValue));
	}

	private void displayName(ActionEvent e) {
		
		System.out.println(model.getName());
		System.out.println(model.getAge());
		System.out.println(model.getMarried());
	
	}

	private void changeName(String newValue) {
		model.setName(newValue);
	}

}
