package Person;
import java.util.Objects;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class PersonModel {
		
		private SimpleIntegerProperty age;
		private SimpleBooleanProperty married;
		private SimpleStringProperty name;
		
		public PersonModel() {
			this.age = new SimpleIntegerProperty();
			this.married = new SimpleBooleanProperty();
			this.name = new SimpleStringProperty();
		}
		
		public int getAge() {
			return age.get();
		}
		
		public void setAge(int newValue) {
			age.set(newValue);
		}
		
		public SimpleIntegerProperty ageProperty() {
			return age;
		}
		
		public boolean getMarried() {
			return married.get();
		}
		
		public void setMarried(boolean newValue) {
			married.set(newValue);
		}
		
		public SimpleBooleanProperty marriedProperty() {
			return married;
		}
		
		public String getName() {
			return name.get();
		}
		
		public void setName(String newValue) {
			name.set(newValue);
		}
		
		public SimpleStringProperty nameProperty() {
			return name;
		}
		
		public int hashCode() {
			return age.hashCode();
		}
}