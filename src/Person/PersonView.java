package Person;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class PersonView {
	
	protected Label nameLabel, marriedLabel, ageLabel;
	protected TextField nameText, marriedText, ageText;
	protected Button button;
	protected Stage stage;
	protected PersonModel model;
	
	public PersonView (Stage stage, PersonModel model) {
		this.stage = stage;
		this.model = model;
		
		VBox root = new VBox();
		
		nameLabel = new Label("Name");
		nameText = new TextField();
		marriedLabel = new Label("Married?");
		marriedText = new TextField();
		ageLabel = new Label("Age");
		ageText = new TextField();
		button = new Button("Click me");
		
		root.getChildren().addAll(nameLabel, nameText, marriedLabel, marriedText, ageLabel, ageText, button);
		
		Scene scene = new Scene(root);
		stage.setTitle("Person");
		stage.setScene(scene);
	}
	
	public void start() {
		stage.show();
	}
}