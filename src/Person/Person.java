package Person;

import javafx.application.Application;
import javafx.stage.Stage;

public class Person extends Application {
	
	protected PersonView view;
	protected PersonModel model;
	protected PersonController controller;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		model = new PersonModel();
		view = new PersonView(stage, model);
		controller = new PersonController(view, model);
		view.start();
	}
}