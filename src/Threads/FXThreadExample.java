package Threads;

import javafx.application.Application;
import javafx.stage.Stage;

public class FXThreadExample extends Application {
	
	private FXThreadExampleView view;
	private FXThreadExampleModel model;
	private FXThreadExampleController controller;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		model = new FXThreadExampleModel();
		view = new FXThreadExampleView(stage, model);
		controller = new FXThreadExampleController(view, model);
		view.start();
	}
}
