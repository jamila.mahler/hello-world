package Threads;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.concurrent.Task;

public class FXThreadExampleModel {
	
	protected SimpleIntegerProperty counter;
	private volatile boolean stop = true;
	
	protected FXThreadExampleModel() {
		counter = new SimpleIntegerProperty(0);
	}
	
	public SimpleIntegerProperty getCounter() {
		return counter;
	}
	
	public void startStop() {
		if (stop) {
			stop = false;
			
			
			CountUpTask c = new CountUpTask();
			c.start();
//			CountUpTask task = new CountUpTask();
//			CountDownTask task1 = new CountDownTask();
//			new Thread(task, "Simulated work").start();
//			new Thread(task1, "Simulated work").start();
		} else {
			stop = true;
		}
	}
	
	public void countDown() {
		CountDownTask c = new CountDownTask();
		c.start();
	}
	
	
	public class CountUpTask extends Thread {

		@Override
		public void run() {
			while (counter.getValue() < 100) {
				counter.set(counter.get() + 1);
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					
				}
			}
		}
		
	}
	
	public class CountDownTask extends Thread {

		public void run() {
			while (counter.getValue() > -100) {
				counter.set(counter.get() - 1);
				try {
					Thread.sleep(100);
				} catch(InterruptedException e) {
					
				}
			}
		}
	}
}