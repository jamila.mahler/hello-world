package Threads;

import javafx.application.Platform;
import javafx.event.ActionEvent;

public class FXThreadExampleController {
	
	private FXThreadExampleView view;
	private FXThreadExampleModel model;
	
	public FXThreadExampleController(FXThreadExampleView view, FXThreadExampleModel model) {
		this.view = view;
		this.model = model;
		
		model.getCounter().addListener((observable, oldValue, newValue) -> {
			updateGUI((Integer) newValue);
			});
		
		view.countDown.setOnAction( e -> {
			model.countDown();
			});
		
		view.countUp.setOnAction( e -> {
			model.startStop();
			});
		
		view.getStage().setOnCloseRequest(e -> {
			view.stop();
		Platform.exit();
		});
		
	}
	
	private void updateGUI(int newValue) {
	Platform.runLater(() -> {view.counterView.setText(
			Integer.toString((Integer) newValue));
	});
	
}

}
