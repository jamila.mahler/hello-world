package Threads;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class FXThreadExampleView {
	
	private Stage stage;
	private FXThreadExampleModel model;
	protected TextField counterView;
	protected Button countUp, countDown;
	
	public FXThreadExampleView(Stage stage, FXThreadExampleModel model) {
		this.stage = stage;
		this.model = model;
		
		VBox root = new VBox();
		
		this.counterView = new TextField();
		counterView.setText(Integer.toString(model.getCounter().get()));
		this.countUp = new Button("Count up!");
		this.countDown = new Button("Count down!");
		root.getChildren().addAll(counterView, countUp, countDown);
		
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle("FX Threads Example");
	}
	
	public void start() {
		stage.show();
	}
	
	public Stage getStage() {
		return this.stage;
	}

	public void stop() {
		stage.hide();
	}

}
