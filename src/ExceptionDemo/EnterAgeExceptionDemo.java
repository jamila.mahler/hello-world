package ExceptionDemo;

import java.util.Scanner;

public class EnterAgeExceptionDemo {

	public static void main(String[] args) throws Exception {
		Scanner s = new Scanner(System.in);
		System.out.println("Please enter your age: ");
		String input = s.nextLine();
		
		try {
			int age = parseUserInput(input);
			System.out.println("Your age is: " + age);
		} catch (Exception e){
			System.out.println("Your input is invalid!");
			System.out.println(e.getMessage());
		}
		try {
			s.close();
		} catch (Exception e) {
			throw new Exception("Failure with closing your ressource.");
		}

	}

	private static int parseUserInput(String input) throws Exception {
		try {
			int value = Integer.parseInt(input);
			if (value < 0 || value > 100) {
				throw new Exception ("Value must be 0 to 100");
				
			}
			return value;
		} catch (NumberFormatException e) {
			throw new Exception ("Your input does not match the needed data type");
		}
	}
}