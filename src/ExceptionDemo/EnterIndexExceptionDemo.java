package ExceptionDemo;

import java.util.Scanner;

public class EnterIndexExceptionDemo {

	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		System.out.println("Geben Sie einen Index ein: ");
		int input = s.nextInt();
		
		try {
		
		int[] array = new int[3];
		array[0] = 56;
		array[1] = 34;
		array[2] = 78;
		System.out.println(array[input]);
		
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Dein Index ist ungültig.");
		} finally {
			System.out.println("Vielen Dank für Ihre Eingaben!");
		}
	}
}
//		
//		try {
//			if (!(input < 0 || input > 100)) {
//				System.out.println("Sie haben ein Alter, welches der definierten Range entspricht.");
//			}
//			
//		} catch (IndexOutOfBoundsException e){
//			System.out.println("Ihre Eingabe ist nicht im definierten Alterrange.");
//		} finally {
//			System.out.println("Vielen Dank für Ihre Angaben.");
//		}
//
//	}
//
//}
