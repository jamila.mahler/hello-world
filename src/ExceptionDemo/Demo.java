package ExceptionDemo;

import java.util.Scanner;

public class Demo {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int choice = -1;
		while (choice != 0) {
			System.out.println("What do you want to do?");
			System.out.println("0 - Exit this program");
			System.out.println("1 - Use an invalid array index");
			System.out.println("2 - Divide-by-zero with int-values");
			System.out.println("3 - Divide-by-zero with float-values");
			System.out.println("4 - Use a null pointer");
			System.out.println("5 - Something else you think should be an exception");
			choice = in.nextInt();
			try {
				System.out.println("Entering 'try'");
				switch (choice) {
				case 0: 
					break;
				case 1:
					int[] array = new int[choice];
					System.out.println(array[choice]);

					break;
				case 2: choice = choice / 0;

					break;
				case 3: float f = choice;
						float e = 0;
						float g = f / e;

					break;
				case 4:
					Object o = null;
					System.out.println(o.toString());

					break;
				case 5: // Something else

					break;
				default:
					System.out.println("Invalid choice, no exception");
				}
			} catch (Exception e) {
				System.out.println("Exception caught: " + e.getClass());
			} finally {
				System.out.println("Executing 'finally'");
			}
		}
		in.close();
	}
}