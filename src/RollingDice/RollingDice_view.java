package RollingDice;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class RollingDice_view {
	
	private Stage primaryStage;
	private RollingDice_model model;
	protected Button b1, b2;
	
	public RollingDice_view (Stage primaryStage, RollingDice_model model) {
		this.primaryStage = primaryStage;
		this.model = model;
		
		HBox root = new HBox();
		b1 = new Button();
		b2 = new Button();
		b1.setId("button");
		b2.setId("button");
		root.getChildren().addAll(b1,b2);
		
		Scene scene = new Scene (root, 200, 100);
		primaryStage.setTitle("Rolling Dice App");
		primaryStage.setScene(scene);
		
		scene.getStylesheets().add(getClass().getResource("rollingDice.css").toExternalForm());
		
	}
	
	public void start() {
		primaryStage.show();	
	}

}
