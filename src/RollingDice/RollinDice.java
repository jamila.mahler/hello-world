package RollingDice;

import javafx.application.Application;
import javafx.stage.Stage;

public class RollinDice extends Application {
	
	private RollingDice_model model;
	private RollingDice_view view;
	private RollingDice_controller controller;

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.model = new RollingDice_model();
		this.view = new RollingDice_view(primaryStage, model);
		this.controller = new RollingDice_controller(view, model);
		view.start();
		
	}

}
