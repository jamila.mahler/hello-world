package RollingDice;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

public class RollingDice_controller {
	
	private RollingDice_view view;
	private RollingDice_model model;
	
	public RollingDice_controller (RollingDice_view view, RollingDice_model model) {
		this.view = view;
		this.model = model;
		
		view.b1.setOnMouseEntered(this::getRandomNumber1);
		view.b2.setOnAction(this::getRandomNumber);
	}
	
	public void getRandomNumber1(MouseEvent e) {
		Button b = (Button) e.getSource();
		b.setText(model.getRandomNumber());	
	}
	
	
	public void getRandomNumber(ActionEvent e) {
		Button b = (Button) e.getSource();
		b.setText(model.getRandomNumber());	
	}
}