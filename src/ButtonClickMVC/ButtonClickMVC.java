package ButtonClickMVC;

import javafx.application.Application;
import javafx.stage.Stage;

public class ButtonClickMVC extends Application {
	
	private ButtonClickView view;
	private ButtonClickController controller;
	private ButtonClickModel model;

	public static void main(String[] args) {
	launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		model = new ButtonClickModel();
		view = new ButtonClickView (primaryStage, model);
		controller = new ButtonClickController (model, view);
		view.start();
	}

}
