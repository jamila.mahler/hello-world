package ButtonClickMVC;

import javafx.event.ActionEvent;

public class ButtonClickController {
	
	private final ButtonClickModel model;
	private final ButtonClickView view;
	
	
	protected ButtonClickController (ButtonClickModel model, ButtonClickView view) {
		this.model = model;
		this.view = view;
		
		view.btnClick.setOnAction(this::clickButton);
	}
	
	protected void clickButton(ActionEvent event) {
		model.incrementValue();
		String newText = Integer.toString(model.getValue());
		view.lblNumber.setText(newText);
	}

}
