package ButtonClickMVC;

import javafx.beans.property.SimpleIntegerProperty;

public class ButtonClickModel {
	
	private final SimpleIntegerProperty value;

	
	protected ButtonClickModel() {
		value = new SimpleIntegerProperty();
		value.setValue(0);
	}
	
	public SimpleIntegerProperty valueProperty() {
		return value;
	}
	
	public int getValue() {
		return value.get();
	}
	
	public int incrementValue() {
		int val = value.get();
		val++;
		value.setValue(val);
		return val;
	}

}
