package ButtonClickMVC;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ButtonClickView {
	
	private final ButtonClickModel model;
	private Stage primaryStage;
	protected Label lblNumber;
	protected Button btnClick;
	
	
	protected ButtonClickView (Stage primaryStage, ButtonClickModel model) {
		this.primaryStage = primaryStage;
		this.model = model;
		primaryStage.setTitle("Button Click View");
		
		GridPane pane = new GridPane();
		lblNumber = new Label();
		lblNumber.setText(Integer.toString(model.getValue()));
		pane.add(lblNumber, 0, 0);
		
		btnClick = new Button();
		btnClick.setText("Click me");
		pane.add(btnClick, 0, 1);
		
		Scene scene = new Scene(pane);
		scene.getStylesheets().add(getClass().getResource("ButtonClickMVC.cs").toExternalForm());
		primaryStage.setScene(scene);
		
	}
	
	public void start() {
		primaryStage.show();
	}
	
	public Stage getStage() {
		return primaryStage;
	}

}
