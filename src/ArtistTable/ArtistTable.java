package ArtistTable;

import javafx.application.Application;
import javafx.stage.Stage;

public class ArtistTable extends Application {
	
	private ArtistTableView view;
	private ArtistTableModel model;
	private ArtistTableController controller;

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage stage) throws Exception {
		model = new ArtistTableModel();
		view = new ArtistTableView(stage, model);
		controller = new ArtistTableController(view, model);
		view.start();
	}

}
