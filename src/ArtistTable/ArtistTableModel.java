package ArtistTable;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ArtistTableModel {
	
	public ObservableList<Artist> elements = FXCollections.observableArrayList();
	
	public ObservableList<Artist> getElements() {
		return elements;
	}
	
	public void addNewArtist() {
		elements.add(new Artist("Marc Hermann","1.8.1978", "dä cha nit singe"));
	}

}
