package ArtistTable;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ArtistTableView {
	
	private ArtistTableModel model;
	private Stage stage;
	private TableView<Artist> tableView;
	protected Button addNewArtist;
	
	public ArtistTableView(Stage stage, ArtistTableModel model) {
		this.model = model;
		this.stage = stage;
		
		VBox root = new VBox();
		tableView = createTableView();
		addNewArtist = new Button("Artist hinzufügen");
		
		root.getChildren().addAll(tableView, addNewArtist);
		
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle("Artist Table");
	}
	
	private TableView<Artist> createTableView() {
		this.tableView = new TableView<>();
		tableView.setEditable(true);
		
		// Spalte für den Namen des Artist
		TableColumn<Artist, String> nameCol = new TableColumn<>("Artist");
		nameCol.setCellValueFactory(c -> c.getValue().nameProperty());
		nameCol.setCellFactory(TextFieldTableCell.forTableColumn());
		tableView.getColumns().add(nameCol);
		
		// Spalte für das Geburtsdatum des Artists
		TableColumn<Artist, String> birthdayCol = new TableColumn<>("Birthday");
		birthdayCol.setCellValueFactory(c -> c.getValue().birthdayProperty());
		birthdayCol.setCellFactory(TextFieldTableCell.forTableColumn());
		tableView.getColumns().add(birthdayCol);
		
		// Spalte für den Lieblingssong
		TableColumn<Artist, String> favSongCol = new TableColumn<>("Favorite Song");
		favSongCol.setCellValueFactory(c -> c.getValue().favSongProperty());
		favSongCol.setMinWidth(100);
		favSongCol.setCellFactory(TextFieldTableCell.forTableColumn());
		tableView.getColumns().add(favSongCol);
		
		tableView.setItems(model.getElements());
		
		return tableView;
	}

	public void start() {
		stage.show();
	}

}
