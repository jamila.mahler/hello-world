package ArtistTable;

import java.io.InputStream;
import java.io.OutputStream;

import javafx.event.ActionEvent;

public class ArtistTableController {
	
	public ArtistTableModel model;
	public ArtistTableView view;
	public InputStream i;
	public OutputStream o;
	
	public ArtistTableController (ArtistTableView view, ArtistTableModel model) {
		this.view = view;
		this.model = model;
		
		view.addNewArtist.setOnAction(this::addNewArtist);
		
		// InputStream
		i = ArtistTableController.class.getResourceAsStream("Artist.txt");
		
	}
	
	public void addNewArtist(ActionEvent e) {
		model.addNewArtist();
	}
	
	
}