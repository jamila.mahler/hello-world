package ArtistTable;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Artist {
	
	public SimpleStringProperty name, birthday, favSong;
	
	
	public Artist(String name, String birthday, String favSong) {
		this.name = new SimpleStringProperty(name);
		this.birthday = new SimpleStringProperty(birthday);
		this.favSong = new SimpleStringProperty(favSong);
	}
	
	public String getName() {
		return name.get();
	}
	
	public String getBirthday() {
		return birthday.get();
	}
	
	public String getFavSong() {
		return favSong.get();
	}
	
	public void setName(String newValue) {
		name.set(newValue);
	}
	
	public void setBirthday(String newValue) {
		birthday.set(newValue);
	}
	
	public void setFavSong(String newValue) {
		favSong.set(newValue);
	}
	
	public SimpleStringProperty nameProperty() {
		return name;
	}
	
	public SimpleStringProperty birthdayProperty() {
		return birthday;
	}
	
	public SimpleStringProperty favSongProperty() {
		return favSong;
	}

}
