package InputOutput;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class CopyFile_view {
	
	public CopyFile_model model;
	public Stage stage;
	public Label instruction, inputFile, outputFile;
	public TextField inputFileText, outputFileText;
	public Button copy;
	
	public CopyFile_view(Stage stage, CopyFile_model model) {
		this.model = model;
		this.stage = stage;
		
		BorderPane root = new BorderPane();
		instruction = new Label("Enter complete paths for the input and output files");
		HBox input = new HBox();
		HBox output = new HBox();
		VBox vbox = new VBox();
		inputFile = new Label("Input File");
		outputFile = new Label("Output File");
		inputFileText = new TextField();
		outputFileText = new TextField();
		copy = new Button("Copy");
		
		input.getChildren().addAll(inputFile, inputFileText);
		output.getChildren().addAll(outputFile, outputFileText);
		vbox.getChildren().addAll(input, output);
		root.setCenter(vbox);
		root.setTop(instruction);
		root.setBottom(copy);
		
		Scene scene = new Scene(root, 200, 200);
		stage.setScene(scene);
		stage.setTitle("File copy");
	}
	
	public void start() {
		stage.show();
	}

}
