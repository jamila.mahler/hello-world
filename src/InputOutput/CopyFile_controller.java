package InputOutput;

public class CopyFile_controller {
	
	public CopyFile_view view;
	public CopyFile_model model;
	
	public CopyFile_controller(CopyFile_view view, CopyFile_model model) {
		this.view = view;
		this.model = model;
	}

}
