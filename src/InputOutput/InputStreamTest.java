package InputOutput;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class InputStreamTest {

	public static void main(String[] args) throws IOException, URISyntaxException {

		
		// access a file in the same Package
		InputStream s = InputStreamTest.class.getResourceAsStream("Test.txt");
		String data;
		try (BufferedReader b = new BufferedReader(new InputStreamReader(s))) {
		data = b.readLine();
		} catch (IOException e) {
			data = e.getClass().toString();
		}
		System.out.println(data);
		
		
		//access a file that is in another package
		InputStream input = InputStreamTest.class.getResourceAsStream("/FilesToAccess/" + "AnotherPackage.txt");
		String data1;
		try (BufferedReader fileIn = new BufferedReader(new InputStreamReader(input))) {
			data1 = fileIn.readLine();
		} catch (IOException e) {
			data1 = e.getClass().toString();
		}
		System.out.println(data1);
		
		
		//access a file from the working directory
		File file = new File("blabla.txt");
		String data2;
		try (BufferedReader fileIn1 = new BufferedReader(new FileReader(file))) {
			data2 = fileIn1.readLine();
		} catch (FileNotFoundException e) {
			data2 = "Save file does not exist";
		} catch (IOException e) {
			data2 = e.getClass().toString();
		}
		System.out.println(data2);
		
		
		//new Version, which reads the whole file and not only one line (from same Package)
				URL url = InputStreamTest.class.getResource("Test.txt");
				Path path = Paths.get(url.toURI());
				List<String> lines = Files.readAllLines(path);
				lines.forEach(System.out::println);
	}

}

