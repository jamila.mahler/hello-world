package InputOutput;

import javafx.application.Application;
import javafx.stage.Stage;

public class CopyFile extends Application {
	
	public CopyFile_view view;
	public CopyFile_model model;
	public CopyFile_controller controller;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		this.model = new CopyFile_model();
		this.view = new CopyFile_view(stage, model);
		this.controller = new CopyFile_controller(view, model);
		view.start();
	}
	


}
