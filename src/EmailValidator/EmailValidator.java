package EmailValidator;

import javafx.application.Application;
import javafx.stage.Stage;

public class EmailValidator extends Application {
	
	private EmailValidator_view view;
	private EmailValidator_model model;
	private EmailValidator_controller controller;
	
	public static void main(String[]args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		model = new EmailValidator_model();
		view = new EmailValidator_view(stage, model);
		controller = new EmailValidator_controller(model, view);
		view.start();
	}
}
