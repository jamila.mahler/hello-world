package EmailValidator;

import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class EmailValidator_view {
	
	private EmailValidator_model model;
	private Stage stage;
	
	protected TextField txtEmail;
	
	public EmailValidator_view(Stage stage, EmailValidator_model model) {
		this.model = model;
		this.stage = stage;
		
		stage.setTitle("EmailValidator");
		
		GridPane root = new GridPane();
		txtEmail = new TextField();
		root.add(txtEmail, 0, 0);
		
		Scene scene = new Scene(root);
		stage.setScene(scene);
		
		scene.getStylesheets().add(
				getClass().getResource("EmailValidator.css").toExternalForm());    
		
	}

	
	public void start() {
		stage.show();
	}
}