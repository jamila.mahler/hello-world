package EmailValidator;

public class EmailValidator_controller {
	
	private EmailValidator_model model;
	private EmailValidator_view view;
	
	public EmailValidator_controller(EmailValidator_model model, EmailValidator_view view) {
		this.model = model;
		this.view = view;
		
		view.txtEmail.textProperty().addListener(
				(observable, oldValue, newValue) -> validateEmailAdress(newValue)
				);
	}
	
	private void validateEmailAdress(String newValue) {
		boolean valid = false;
		
		String[] addressParts = newValue.split("@");
		
		if(addressParts.length == 2 && !addressParts[0].isEmpty() && !addressParts[1].isEmpty()) {
			
			if (addressParts[1].charAt(addressParts[1].length() -1) != '.') {
				
				String[]domainParts = addressParts[1].split("\\.");
				if (domainParts.length >= 2) {
					valid = true;
					for (String s : domainParts) {
						if (s.length() < 2) valid = false;
					}
				}
			}
		}
	
	view.txtEmail.getStyleClass().remove("emailNotOk");
	view.txtEmail.getStyleClass().remove("emailOk");
	
	if (valid) {
		view.txtEmail.getStyleClass().add("emailOk");
	} else {
		view.txtEmail.getStyleClass().add("emailNotOk");
	}
}	
}