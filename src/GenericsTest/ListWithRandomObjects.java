package GenericsTest;

import java.util.ArrayList;

public class ListWithRandomObjects {
	
	public static void main(String[] args) {
		
		Object o = new Object();
		Integer i = new Integer(5);
		Number s = new Double(59.256);
		
		ArrayList<? super Number> randomObjects = new ArrayList<>();
		randomObjects.add(i);
		randomObjects.add(s);
		
		for (Object r : randomObjects) {
			System.out.println(r);
		}
	}

}
