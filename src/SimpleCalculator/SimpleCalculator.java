package SimpleCalculator;

import javafx.application.Application;
import javafx.stage.Stage;

public class SimpleCalculator extends Application {
	
	protected SimpleCalculatorView view;
	private SimpleCalculatorModel model;
	private SimpleCalculatorController controller;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		model = new SimpleCalculatorModel();
		view = new SimpleCalculatorView(stage, model);
		controller = new SimpleCalculatorController(model, view);
		view.start();
	}

}
