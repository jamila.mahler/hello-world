package SimpleCalculator;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class SimpleCalculatorView {
	
	Stage stage;
	SimpleCalculatorModel model;
	Button[] digits;
	Button plus, cancel, equals;
	TextField entryField;
	GridPane digitsGrid;
	
	public SimpleCalculatorView (Stage stage, SimpleCalculatorModel model) {
		this.stage = stage;
		this.model = model;
		
		stage.setTitle("Simple Calculator");
		BorderPane root = new BorderPane();
		entryField = new TextField();
		plus = new Button("+");
		cancel = new Button ("C");
		equals = new Button("=");
		
		VBox v = new VBox();
		v.getChildren().addAll(plus, cancel, equals);
		
		root.setTop(entryField);
		root.setRight(v);
		
		digitsGrid = new GridPane();
		digits = new Button[10];
		
		// for-Schleife to create buttons
		for (int i = 0; i < digits.length; i++) {
			digits[i] = new Button(Integer.toString(i));
		}
		
		digitsGrid.add(digits[0], 0, 4, 1, 3);
		
		// for-Schleife to add buttons to root
		for (int j = 1; j < digits.length; j++) {
			digitsGrid.add(digits[j], (j-1)%3, (j-1) / 3);
		}
		
		root.setCenter(digitsGrid);
		
		Scene scene = new Scene(root, 300, 300);
		stage.setScene(scene);
		
		scene.getStylesheets().add(getClass().getResource("Calculator_CSS.css").toExternalForm());
		plus.getStyleClass().add("test");
		
		plus.setId("button");
		equals.setStyle("-fx-text-fill: red;");
		
	}

	public void start () {
		stage.show();
	}
	
}

