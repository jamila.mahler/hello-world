package SimpleCalculator;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;

public class SimpleCalculatorController {
	
	SimpleCalculatorModel model;
	SimpleCalculatorView view;
	
	public SimpleCalculatorController (SimpleCalculatorModel model,  SimpleCalculatorView view) {
		this.model = model;
		this.view = view;
		
		view.cancel.setOnAction(this::cancelAllEntries);
		
		for (Button b : view.digits) {
			b.setOnAction(this::addValueToButtonField);
		}
		
	}
	
	public void addValueToButtonField (ActionEvent e) {
		Button b = (Button) e.getSource();
		view.entryField.setText(view.entryField.getText() + b.getText());
	}
	
	public void cancelAllEntries (ActionEvent e) {
		model.empty(view.entryField);
	}

}
