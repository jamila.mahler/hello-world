package JUnitTesting;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MyMethosTestCaseSquare {

	@Test
	void test() {
		MyMethods m = new MyMethods();
		int output = m.square(5);
		assertEquals(37, output);
	}

}
