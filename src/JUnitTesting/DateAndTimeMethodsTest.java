/**
 * 
 */
package JUnitTesting;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author jamilamahler
 *
 */
class DateAndTimeMethodsTest {

	@Test
	public void testGetAgeBasic() {
		
		LocalDate onDate = LocalDate.of(2015, 6, 15);
		
		try {
		assertEquals(DateAndTimeMethods.getAge(LocalDate.of(2010, 3, 3), onDate), 5);
		assertEquals(DateAndTimeMethods.getAge(LocalDate.of(2010, 8, 8), onDate), 4);
		assertEquals(DateAndTimeMethods.getAge(LocalDate.of(2010, 6, 3), onDate), 5);
		assertEquals(DateAndTimeMethods.getAge(LocalDate.of(2010, 6, 23), onDate), 4);
		
	} catch(Exception e) {
		fail();
	}
	}
	
	public void testGetAgeNegative() {
		LocalDate onDate = LocalDate.of(2015, 6, 15);
		LocalDate birthDate = LocalDate.of(2020, 7, 23);
		
		try {
			DateAndTimeMethods.getAge(birthDate, onDate);
			fail();
		} catch (Exception e) {
			assertTrue(true);
			e.getMessage();
		}
	}

}
