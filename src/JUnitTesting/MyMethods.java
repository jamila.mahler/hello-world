package JUnitTesting;

public class MyMethods {

	public int square(int x) {
		return x*x;
	}
	
	public int countA (String word) {
		int count = 0;
		for (int i = 0; i < word.length(); i++) {
			if (word.charAt(i) == 'a' || word.charAt(i) == 'A') {
				count++;
			}
		}
		return count;	
	}
	
	public boolean ChooseRightNumber(int input) {
		boolean right = false;
		if (input == 5) {
			right = true;
		}
		return right;
	}
}