package JUnitTesting;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class PersonTest {

	@ParameterizedTest
	@MethodSource ("personParameters")
	void testCelebrateBirthday(Person person, int age) {
		person.celebrateBirthday();
		assertEquals(person.getAge(), age);
	
	}

	public static Stream<Arguments> personParameters () {
		return Stream.of(
				Arguments.of(new Person("Marc Hermann", 38), 39),
				Arguments.of(new Person ("Jamila Mahler", 26), 27),
				Arguments.of(new Person ("Michael Kalt", 32), 33)
		);
	}
}
