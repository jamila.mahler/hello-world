package JUnitTesting;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MyMethodsTestCaseFindA {

	@Test
	void test() {
		MyMethods m = new MyMethods();
		int output = m.countA("Javabean");
		assertEquals(2, output);
	}

}
