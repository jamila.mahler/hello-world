import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ButtonDemo extends Application {
	
	Integer initialValue = 1;
	Button button = new Button("Increment");
	Label l = new Label();

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		VBox root = new VBox();
		
		root.getChildren().addAll(l, button);
		setValue();
		
		Scene scene = new Scene(root, 150, 150);
		primaryStage.setScene(scene);
		primaryStage.show();
		
		button.setOnAction(this::incrementValue);
	}
	
	public void setValue() {
		l.setText(initialValue.toString());
	}
	
	public void incrementValue(ActionEvent e) {
		initialValue++;
		setValue();
	}
	
	public String getValue() {
		return l.getText();
	}
	
}
