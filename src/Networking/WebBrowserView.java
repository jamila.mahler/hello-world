package Networking;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class WebBrowserView {
	
	Stage stage;
	WebBrowserModel model;
	Label lblIpAddress, lblPort;
	TextField txtIpAddress, txtPort;
	TextArea txtWebPage;
	Button go;
	
	public WebBrowserView (Stage stage, WebBrowserModel model) {
		this.stage = stage;
		this.model = model;
		
		HBox root = new HBox();
		lblIpAddress = new Label("IP Address");
		lblPort = new Label ("Port Number");
		txtIpAddress = new TextField();
		txtPort = new TextField();
		go = new Button("Verbinden.");
		txtWebPage = new TextArea();
		root.getChildren().addAll(lblIpAddress, txtIpAddress, lblPort, txtPort, go, txtWebPage);
		
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle("WebBrowser");
	}
	
	public void start() {
		stage.show();
	}

}
