package Networking;

import javafx.event.ActionEvent;

public class WebBrowserController {
	
	WebBrowserView view;
	WebBrowserModel model;
	
	public WebBrowserController(WebBrowserModel model, WebBrowserView view) {
		this.model = model;
		this.view = view;
		
		view.go.setOnAction(this::connectToWebBrowser);
	}
	
	public void connectToWebBrowser(ActionEvent e) {
		String ipAddress = view.txtIpAddress.getText();
		Integer port = Integer.parseInt(view.txtPort.getText());
		String webPage = model.browse(ipAddress, port);
		view.txtWebPage.setText(webPage);
	}

}
