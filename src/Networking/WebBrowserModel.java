package Networking;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class WebBrowserModel {

	public String browse(String ipAddress, Integer port) {
		String lineIn;
		StringBuffer urlContent = new StringBuffer();
		
		try (Socket s = new Socket(ipAddress, port);
				OutputStreamWriter out = new OutputStreamWriter(s.getOutputStream());
				BufferedReader inReader = new BufferedReader(new InputStreamReader(s.getInputStream()));
				) {
			out.write("GET / HTTP/1.0\r\n");
			out.write("User Agent: Broswer0\r\n");
			out.write("Host: " + ipAddress + ": " + port + "\r\n");
			out.write("Accept: text/html, */*\r\n\r\n");
			out.flush();
			
			while((lineIn = inReader.readLine()) != null) {
				urlContent.append(lineIn + "\n");
			}
			
		} catch(Exception err) {
			urlContent.append("ERROR: " + err.toString());
		}
		
		return urlContent.toString();
	}
	

}
