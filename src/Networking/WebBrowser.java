package Networking;

import javafx.application.Application;
import javafx.stage.Stage;

public class WebBrowser extends Application {

	private WebBrowserModel model;
	private WebBrowserView view;
	private WebBrowserController controller;
	
	public static void main(String[]args) {
		launch();
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		
		this.model = new WebBrowserModel();
		this.view = new WebBrowserView(stage, model);
		this.controller = new WebBrowserController(model, view);
		view.start();
	}
	
	
		

}
