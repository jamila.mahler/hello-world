package Probeprüfungen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Aufgabe2_FS2019 {

	public static void main(String[] args) {
		ArrayList<String> names = null;
		
		try {
			System.out.print("1");
			System.out.println(names.size());
		} catch (Exception e) {
			System.out.print(2);
		} finally {
			System.out.println(3);
		}
		
		int limit = 3;
		try {
			woof(limit);
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		List<String> nums = Arrays.asList("2", "5", "11");
		Collections.sort(nums);
		for (String s : nums) System.out.print(s);
		
		int[] nums1 = new int[] {2,3,5,7,11,13};
		int pos = 0;
		while (pos <= nums1.length) {
			pos = nums1[pos];
			System.out.print(pos);
		}
		
		System.out.println();
		
		for (int i = 1; i<4; i++) {
			for (int j = 0; j<i; j++) {
				System.out.print(i + j);
			}
		}

	}
	
	public static void woof (int limit) throws Exception {
		for (int i = 1; i <5; i++) {
			System.out.print(i);
			if (i >= limit) throw new Exception ("boom");
		}
	}

}
