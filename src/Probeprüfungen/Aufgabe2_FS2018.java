package Probeprüfungen;

public class Aufgabe2_FS2018 {

	public static void main(String[] args) {
		
		String s1 = "aaa";
		String s2 = "bbb";
		
		s1 = s2;
		
		System.out.println((s1 == s2) + " " + (s1.equals(s2)));
		
		System.out.println(calc(1));
		System.out.println(calc(3));
	}

	private static int calc(int x) {
		if (x <= 0) {
			return 0;
		} else {
			return 2*x + calc(x-1);
		}
	}

}
