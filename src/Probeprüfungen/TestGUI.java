package Probeprüfungen;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TestGUI extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		
		BorderPane root = new BorderPane();
		VBox v = new VBox(30);
		HBox h = new HBox(40);
		ObservableList<Integer> arrayList = FXCollections.observableArrayList();
		ListView list = new ListView<>(arrayList);
		
		Button b = new Button("Ich bin ein Button");
		Button vbox = new Button("Ich bin ein Button in der VBox");
		Label ha = new Label ("Ich heisse h.");
		Label l = new Label("Ich stehe einfach mal hier");
		Label hallo = new Label("Hallo");
		
		root.setLeft(v);
		root.setRight(h);
		v.getChildren().add(hallo);
		BorderPane.setAlignment(hallo, Pos.CENTER);
		BorderPane.setAlignment(v, Pos.CENTER);
		v.setVgrow(vbox, Priority.ALWAYS);
		v.setVgrow(h, Priority.ALWAYS);
		h.setMargin(ha, new Insets(40, 12, 40, 100));
		h.getChildren().addAll(vbox, ha, b);
		root.setCenter(list);
		root.setBottom(l);
		v.setPadding(new Insets(40,52,89,23));
		
		Scene scene = new Scene(root,300,300);
		stage.setScene(scene);
		stage.setTitle("Test GUI");
		stage.show();
	}

}
