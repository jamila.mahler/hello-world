package Probeprüfungen;

import java.util.TreeSet;

public class Aufgabe2_FS2015 {

	public static void main(String[] args) {
		TreeSet tree = new TreeSet();
		tree.add("Jamila");
		tree.add("Jane");
		tree.add("Claudio");
		tree.add("Anna");
		
		for (Object s : tree) {
			System.out.println(s);
		}

	}

}
