package TicTacToe;

import ButtonClickMVC.ButtonClickController;
import ButtonClickMVC.ButtonClickModel;
import ButtonClickMVC.ButtonClickView;
import javafx.application.Application;
import javafx.stage.Stage;

public class TicTacToe extends Application {
	
	private TTT_view view;
	private TTT_model model;
	private TTT_controller controller;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		model = new TTT_model();
		view = new TTT_view (primaryStage, model);
		controller = new TTT_controller (view, model);
		view.start();
	}
}
