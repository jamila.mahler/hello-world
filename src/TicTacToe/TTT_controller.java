package TicTacToe;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;

public class TTT_controller {
	
	private TTT_view view;
	private TTT_model model;
	static int player = 2;
	
	public TTT_controller(TTT_view view, TTT_model model) {
		this.view = view;
		this.model = model;
		
		
		for (int col = 0; col < view.buttons.length; col++) {
			for (int row = 0; row < view.buttons[0].length; row++) {
				view.buttons[col][row].setOnAction(this::setPlayerOnButtonClicked);
			}
		}
		
	}
	
	public void setPlayerOnButtonClicked (ActionEvent e) {
		Button b = (Button) e.getSource();
		int col = -1;
		int row = -1;
		for (int c = 0; c < view.buttons.length; c++) {
			for (int r = 0; r < view.buttons[0].length; r++) {
				if (b == view.buttons[c][r]) {
					col = c;
					row = r;
				}
			}
		}
		if (model.makeMove(col, row)) {
			view.buttons[col][row].setText(model.board[col][row].toString());
			
			if (model.getWinner() != null) {
				Alert showWinner = new Alert(AlertType.INFORMATION);
				showWinner.setTitle("Gewinner!!!!");
				if (model.getWinner()!= null) {
					showWinner.setHeaderText("Der Gewinner ist: " + model.getWinner());
				}
				if (model.getWinner() == null){
					showWinner.setHeaderText("Unentschieden!! Spielt noch einmal!");
				}
				showWinner.showAndWait();
			}
		}
	}

}
