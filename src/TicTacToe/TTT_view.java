package TicTacToe;

import java.awt.Dimension;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;

public class TTT_view {
	
	private TTT_model model;
	private Stage primaryStage;
	protected Button buttons[][];
	protected GridPane root;
	
	public TTT_view(Stage primaryStage, TTT_model model) {
		this.primaryStage = primaryStage;
		this.model = model;
		
		root = new GridPane();
		buttons = new Button[3][3];
			for (int i = 0; i < buttons.length; i++) {
				for (int j = 0; j < buttons.length; j++) {
					buttons[i][j] = new Button();
					root.add(buttons[i][j],i,j);
					buttons[i][j].setPrefSize(75.0, 75.0);
				}
		}
			
		Scene scene = new Scene (root, 225, 225);
		primaryStage.setScene(scene);
		primaryStage.setTitle("TicTacToe");
		
		scene.getStylesheets().add(getClass().getResource("TicTacToe.css").toExternalForm());
	}
	
	public void start() {
		primaryStage.show();
	}
}
