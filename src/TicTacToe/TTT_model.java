package TicTacToe;

public class TTT_model {
	
	public final static int BOARD_SIZE = 3;
	protected int col, row;
	protected enum validMoves {X,O};
	protected validMoves winner = null;
	protected validMoves[][] board = new validMoves[BOARD_SIZE][BOARD_SIZE];
	protected validMoves nextMove = validMoves.X;
	
	public boolean makeMove(int col, int row) {
		boolean moveMade = false;
		
		if (winner == null && board[col][row] == null) {
		 board[col][row] = nextMove;
		 nextMove = (nextMove == validMoves.X)? validMoves.O : validMoves.X;
		 moveMade = true;
		}
		checkWinner();
		return moveMade;
	}

	public void checkWinner() {
		
		for (int i = 0; i< board.length && winner == null; i++) {
			
			boolean rowWinner = true;
			boolean colWinner = true;
			
			for (int j = 1; j< board[0].length; j++) {
				rowWinner &= board[i][0] != null && board[i][0] == board[i][j];
				colWinner &= board[0][i] != null && board[0][i] == board[i][j];
			}
			if (rowWinner) winner = board[i][0];
			if (colWinner) winner = board[0][i];
		}
		
		boolean digitWinner1 = true;
		boolean digitWinner2 = true;
		for (int i = 1; i<BOARD_SIZE; i++) {
				digitWinner1 &= board[0][0] != null && board[0][0] == board[i][i];
				digitWinner2 &= board[i-1][BOARD_SIZE -i] != null && board[i][BOARD_SIZE -i] == board[BOARD_SIZE -i][BOARD_SIZE -i-1];
				
				if (digitWinner1) winner = board[i][i];
				if (digitWinner2) winner = board[i-1][BOARD_SIZE -i];
 		}
		
		if (winner == null) winner = null;
	}
			
	public validMoves getWinner() {
		return winner;
	}

}
