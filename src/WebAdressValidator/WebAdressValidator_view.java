package WebAdressValidator;


import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class WebAdressValidator_view {
	
	private WebAdressValidator_model model;
	private Stage stage;
	protected Button connect;
	protected Label address, port;
	protected TextField addressText, portText;
	
	public WebAdressValidator_view (Stage stage, WebAdressValidator_model model) {
		this.stage = stage;
		this.model = model;
		
		stage.setTitle("Web Adress Validator");
		
		HBox root = new HBox();
		address = new Label("Web Address: ");
		port = new Label("Port: ");
		addressText = new TextField();
		portText = new TextField();
		connect = new Button("Connect");
		root.setMargin(connect, new Insets(0,0,0,60));
		root.getChildren().addAll(address, addressText, port, portText, connect);
		
		Scene scene = new Scene(root);
		stage.setScene(scene);
		
		scene.getStylesheets().add(getClass().getResource("WebValidator.css").toExternalForm());
		
	}
	
	public void start() {
		stage.show();
	}

}
