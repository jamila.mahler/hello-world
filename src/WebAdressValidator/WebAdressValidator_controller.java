package WebAdressValidator;

public class WebAdressValidator_controller {

	private WebAdressValidator_view view;
	private WebAdressValidator_model model;
	
	private boolean webAddressValid = false;
	private boolean portValid = false;

	public WebAdressValidator_controller(WebAdressValidator_model model, WebAdressValidator_view view) {
		this.model = model;
		this.view = view;

		view.addressText.textProperty().addListener((observable, oldValue, newValue) -> validateWebAddress(newValue));
		view.portText.textProperty().addListener((observable, oldValue, newValue) -> validatePort(newValue));
		view.addressText.textProperty().addListener((observable, oldValue, newValue) -> visableText(newValue));
	}
	
	
	
	private void visableText(String newValue) {
		
		if (view.addressText.getText().contains("karakun")) {
			view.addressText.setVisible(false);
		}	
	}

	// Styles für validierte Web Adressen
	private void validateWebAddress(String newValue) {
		boolean valid = isValidWebAddress(newValue);
		
		view.addressText.getStyleClass().remove("webOk");
		view.addressText.getStyleClass().remove("webNotOk");

		if (valid) {
			view.addressText.getStyleClass().add("webOk");
		} else {
			view.addressText.getStyleClass().add("webNotOk");
		}
		
		webAddressValid = valid;
		
		enableDisableButton();

	}
	
	// Styles für validierte Ports
	private void validatePort(String newValue) {
		boolean valid = isValidPort(newValue);
		
		view.portText.getStyleClass().remove("webOk");
		view.portText.getStyleClass().remove("webNotOk");

		if (valid) {
			view.portText.getStyleClass().add("webOk");
		} else {
			view.portText.getStyleClass().add("webNotOk");
		}
		
		portValid = valid;
		
		enableDisableButton();
	}
	
	// checken, ob die Web Adresse valid ist gemäss Vorgaben
	public boolean isValidWebAddress(String newValue) {
		boolean valid = false;
		String[] n = newValue.split("\\.");
		
		if (n.length == 4) {
			valid = true;
			for (String na : n) {
				try {
					int value = Integer.parseInt(na);
					if (value < 0 || value > 255)
						valid = false;
				} catch (NumberFormatException e) {
					valid = false;
				}
			}
		}
		
		if (!valid) {
			if (n.length >= 2) {
				valid = true;
				for (String na : n) {
					if (na.length() < 2)
						valid = false;
				}
			}
		}
		return valid;
	}
	
	// checken, ob der Port valid ist gemäss Vorgaben
	public boolean isValidPort(String newValue) {
		boolean valid = true;
		
		try {
			int value = Integer.parseInt(newValue);
			if (value < 1 || value > 65535)
				valid = false;
		} catch (NumberFormatException e) {
			valid = false;
		}
		
		return valid;
	}
	
	// Connect Button soll entweder disabled oder enabled sein
	public void enableDisableButton() {
		boolean valid = webAddressValid & portValid;
		view.connect.setDisable(!valid);
	}
}
