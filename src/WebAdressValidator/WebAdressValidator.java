package WebAdressValidator;

import javafx.application.Application;
import javafx.stage.Stage;

public class WebAdressValidator extends Application {
	
	private WebAdressValidator_view view;
	private WebAdressValidator_model model;
	private WebAdressValidator_controller controller;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		view = new WebAdressValidator_view (stage, model);
		model = new WebAdressValidator_model();
		controller = new WebAdressValidator_controller(model, view);
		view.start();
	}

}
