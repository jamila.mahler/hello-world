
public class DebugTest {

	public static void main(String[] args) {
		
		int[] numsIn;
		int[] numsOut;
		
		numsIn = new int[args.length];
		for (int i = 0; i < args.length; i++) {
			numsIn[i] = Integer.parseInt(args[i]);
		}
		
		int lengthNumsOut = numsIn.length/2;
		numsOut = new int[lengthNumsOut];
		
		for (int i = 0; i<lengthNumsOut; i++) {
			numsOut[i] = numsIn[i] + numsIn[numsIn.length - i];
		}
		
		for (int n : numsOut) {
		System.out.println(n);
	}

	}
}
