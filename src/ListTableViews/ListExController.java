package ListTableViews;

import javafx.event.ActionEvent;

public class ListExController {
	
	public ListExView view;
	public ListExModel model;
	
	public ListExController (ListExView view, ListExModel model) {
		this.view = view;
		this.model = model;
		
		view.addNewElement.setOnAction(this::addNewElement);
		view.multiply.setOnAction(this::caluclateMultiply);
	}
	
	public void addNewElement(ActionEvent e) {
		model.addNewElement();
	}
	
	public void caluclateMultiply(ActionEvent e) {
		Integer value = view.listView.getSelectionModel().getSelectedItem();
		if (value != null) {
			Integer result = model.multiplyByTwo(value);
			view.result.setText(result.toString());
		}
	}

}
