package ListTableViews;

import javafx.event.ActionEvent;

public class TableExController {
	
	public TableExView view;
	public TableExModel model;
	
	public TableExController(TableExView view, TableExModel model) {
		this.view = view;
		this.model = model;
		
		view.b.setOnAction(this::addNewElement);
	
	}
	
	public void addNewElement(ActionEvent e) {
		model.addNewElement();
	}

}
