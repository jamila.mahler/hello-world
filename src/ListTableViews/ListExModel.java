package ListTableViews;

import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ListExModel {
	
	public ObservableList<Integer> arrayList = FXCollections.observableArrayList();
	
	public void addNewElement() {
		arrayList.add(arrayList.size());
	}
	
	public int multiplyByTwo(int input) {
		return input*2;
	}
	
	public ObservableList<Integer> getElements () {
		return arrayList;
	}

}
