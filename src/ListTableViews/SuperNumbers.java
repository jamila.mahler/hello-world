package ListTableViews;

import javafx.beans.property.SimpleStringProperty;

public class SuperNumbers {
	
	private Integer value = 0;
	private final SimpleStringProperty asDecimal = new SimpleStringProperty();
	private final SimpleStringProperty asBinary = new SimpleStringProperty();
	private final SimpleStringProperty asHexadecimal = new SimpleStringProperty();
	
	public SuperNumbers(int value) {
		this.value = value;
		updateRepresentations();
	}
	
	public void updateRepresentations() {
		asDecimal.setValue(value.toString());
		asBinary.setValue(Integer.toBinaryString(value));
		asHexadecimal.setValue(Integer.toHexString(value));
	}
	
	public SimpleStringProperty asDecimalProperty() {
		return asDecimal;
	}

	public String getAsDecimal() {
		return asDecimal.get();
	}
	
	public void setAsDecimal(String newValue) {
		value = Integer.parseInt(newValue);
		updateRepresentations();
	}

	public SimpleStringProperty asBinaryProperty() {
		return asBinary;
	}
	
	public String getAsBinary() {
		return asBinary.get();
	}

	public void setAsBinary(String newValue) {
		value = Integer.parseInt(newValue, 2);
		updateRepresentations();
	}

	public SimpleStringProperty asHexadecimalProperty() {
		return asHexadecimal;
	}
	
	public String getAsHexadecimal() {
		return asHexadecimal.get();
	}

	public void setAsHexadecimal(String newValue) {
		value = Integer.parseInt(newValue, 16);
		updateRepresentations();
	}

}
