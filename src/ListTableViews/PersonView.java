package ListTableViews;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class PersonView {
	
	private PersonModel model;
	private Stage stage;
	public TableView<PersonInformation> tableView;
	
	public PersonView(Stage stage, PersonModel model) {
		this.stage = stage;
		this.model = model;
		
		tableView = new TableView<PersonInformation>();
		
		TableColumn<PersonInformation, String> name = new TableColumn<>("Name");
		TableColumn<PersonInformation, Integer> age = new TableColumn<>("Alter");
		
		tableView.getColumns().add(name);
		tableView.getColumns().add(age);
		
		VBox root = new VBox();
		
		root.getChildren().add(tableView);
		
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle("Person");
		
		ObservableList<PersonInformation> persons = FXCollections.observableArrayList();
		
		tableView.setItems(persons);
		
	}
	
	public void start() {
		stage.show();
	}

}
