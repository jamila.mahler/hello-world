package ListTableViews;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class TableExModel {
	
	public ObservableList<SuperNumbers> elements = FXCollections.observableArrayList();
	
	public void addNewElement() {
		elements.add(new SuperNumbers(elements.size()));
	}
	
	public ObservableList<SuperNumbers> getElements() {
		return elements;
	}

}
