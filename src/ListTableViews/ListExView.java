package ListTableViews;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ListExView {
	
	public ListExModel model;
	public Stage stage;
	public Button multiply, addNewElement;
	public Label result;
	public ListView<Integer> listView;
	
	public ListExView(ListExModel model, Stage stage) {
		this.stage = stage;
		this.model = model;
		
		listView = new ListView<>(model.getElements());
		
		VBox root = new VBox();
		VBox.setVgrow(listView, Priority.ALWAYS);
		
		multiply = new Button("Multiply by 2");
		addNewElement = new Button("Add New Element");
		result = new Label();
		
		HBox.setHgrow(result, Priority.ALWAYS);
		HBox h = new HBox(multiply, result);
		
		root.getChildren().addAll(listView,addNewElement, h);
		
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle("List view Example");
		
	}
	
	public void start() {
		stage.show();
	}

}
