package ListTableViews;

public class PersonController {
	
	public PersonView view;
	public PersonModel model;
	
	public PersonController(PersonView view, PersonModel model) {
		this.view = view;
		this.model = model;
	}

}
