package ListTableViews;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TableExView {
	
	public TableExModel model;
	public Stage stage;
	public Button b;
	public TableView<SuperNumbers> tableview;
	
	public TableExView(Stage stage, TableExModel model) {
		this.stage = stage;
		this.model = model;
		
		VBox root = new VBox();
		
		tableview = createTableView();
		b = new Button("Add New Element");
		
		root.getChildren().addAll(tableview, b);
		
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle("Table view Example");
	}
	
	private TableView<SuperNumbers> createTableView() {
		tableview = new TableView<>();
		tableview.setEditable(true);
		
		//Spalte mit binär Zahlen
		TableColumn<SuperNumbers, String> colBinary = new TableColumn<>("Binary");
		colBinary.setCellFactory(TextFieldTableCell.forTableColumn());
		colBinary.setCellValueFactory(c -> c.getValue().asBinaryProperty());
		tableview.getColumns().add(colBinary);
		
		//Spalte mit Dezimalzahlen
		TableColumn<SuperNumbers, String> colDecimal = new TableColumn<>("Decimal");
		colDecimal.setCellValueFactory(c -> c.getValue().asHexadecimalProperty());
		tableview.getColumns().add(colDecimal);
		
		//Spalte mit Hexadecimalzahlen
		TableColumn<SuperNumbers, String> colHex = new TableColumn<>("Hexadecimal");
		colHex.setCellValueFactory(c -> c.getValue().asHexadecimalProperty());
		tableview.getColumns().add(colHex);
		
		tableview.setItems(model.getElements());
		
		return tableview;
	}
	
	public void start() {
		stage.show();
	}

}
