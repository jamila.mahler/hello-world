package ListTableViews;

import java.util.Optional;

import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;

public class AlertTest extends Application {

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage stage) throws Exception {
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Test Alert");
		alert.setHeaderText("Warnung!");
		alert.setContentText("Diese Warnung informiert sie über xy");
		alert.showAndWait();
		
		ChoiceDialog choicedialog = new ChoiceDialog("Auswahl1", 459, "eifach so");
		choicedialog.setTitle("Test Choice Dialog");
		choicedialog.setHeaderText("Auswahlmöglichkeiten!");
		choicedialog.setContentText("Wählen sie aus folgendem Dropdown Menü aus");
		choicedialog.showAndWait();
		
		TextInputDialog text = new TextInputDialog();
		text.setTitle("TextInput Test");
		text.setHeaderText("Text Eingabe");
		text.setContentText("Geben Sie einen gewählten Text hier ein");
		text.showAndWait();
	}
	

}
