package ListTableViews;

import javafx.application.Application;
import javafx.stage.Stage;

public class ListEx extends Application {
	
	public ListExView view;
	public ListExModel model;
	public ListExController controller;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		model = new ListExModel();
		view = new ListExView(model,stage);
		controller = new ListExController(view, model);
		view.start();
	}

}
