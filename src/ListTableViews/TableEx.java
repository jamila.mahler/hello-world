package ListTableViews;

import javafx.application.Application;
import javafx.stage.Stage;

public class TableEx extends Application {
	
	public TableExView view;
	public TableExModel model;
	public TableExController controller;

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage stage) throws Exception {
		
		model = new TableExModel();
		view = new TableExView(stage, model);
		controller = new TableExController(view, model);
		view.start();
	}

}
