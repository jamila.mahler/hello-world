package ListTableViews;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;

public class PersonInformation {
	
	private SimpleIntegerProperty age;
	private StringProperty name;
	
	public PersonInformation(SimpleIntegerProperty age, StringProperty name) {
		this.age = age;
		this.name = name;
	}

}
