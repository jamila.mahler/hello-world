package dataStrucutres;

import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;

public class SampleArray {

	public static void main(String[] args) {
	
	SampleData o1, o2, o3, o4, o5;
	SampleData[] array = new SampleData[5];
	HashMap<Integer, SampleData> hashmap = new HashMap<>();
	TreeSet<SampleData> tree = new TreeSet();

	o1 = new SampleData();
	o2 = new SampleData();
	o3 = new SampleData();
	o4 = new SampleData();
	o5 = new SampleData();
	
	array[0] = o1;
	array[1] = o2;
	array[2] = o3;
	array[3] = o4;
	array[4] = o5;
	
	hashmap.put(0,o1);
	hashmap.put(1, o2);
	hashmap.put(2, o3);
	hashmap.put(3, o4);
	hashmap.put(4, o5);
	
	tree.add(o1);
	tree.add(o2);
	tree.add(o3);
	tree.add(o4);
	tree.add(o5);
	
	Iterator<SampleData> t = tree.iterator();
	
	while (t.hasNext()) {
		System.out.println(t);
	}
	
	
	}
}
